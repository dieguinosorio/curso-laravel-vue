<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $cEmail = $request->cEmail;
        $cContrasena = $request->cContrasena;
        $rpta = \Auth::attempt(['email'=>$cEmail,'password'=>$cContrasena,'state'=>'1']);
        if($rpta){
            return [
                'authUser'=> \Auth::user(),
                'code'=>200
            ];
        }
        else{
            return [
                'code'=>401
            ];
        }
    }

    public function logout(Request $request){
        \Auth::logout();
        return ['code'=>204];
    }
}
