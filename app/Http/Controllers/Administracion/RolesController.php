<?php

namespace App\Http\Controllers\Administracion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class RolesController extends Controller
{
    public function ObtenerListaRoles(Request $request){
        if(!$request->ajax()) return redirect("/");

        $nIdRol = $request->nIdRol;
        $cNombre = $request->cNombre ;
        $cUrl = $request->cUrl;

        $nIdRol = $request->nIdRol == '' ? "0":$request->nIdRol;
        $cNombre = $request->cNombre == '' ? "":$cNombre;
        $cUrl = $request->cUrl == ''  ? "":$cUrl;

        $Datos =  DB::select("call sp_Rol_getListaRoles(?,?,?)",[$cNombre,$cUrl,$nIdRol]);
        
        return ['roles'=>$Datos];
    }

    public function getListarPermisosByRol(Request $request)
    {
        if(!$request->ajax()) return redirect("/");

        $nIdRol = $request->nIdRol;
        $nIdRol = $request->nIdRol == '' ? "0":$nIdRol;
        $Datos =  DB::select("call sp_Rol_getListarPermisoByRoles(?)",[$nIdRol]);
        
        return ['permisosbyrol'=>$Datos];
    }

    public function getListarPermisosByIdRol(Request $request)
    {
        if(!$request->ajax()) return redirect("/");

        $nIdRol = $request->nIdRol;
        $nIdRol = $request->nIdRol == '' ? "0":$nIdRol;
        $Datos =  DB::select("call sp_Rol_getListarPermisoByRolesId(?)",[$nIdRol]);
        
        return ['permisosbyrol'=>$Datos];
    }
    

    public function setRegistrarRolPermiso(Request $request)
    {
        if(!$request->ajax()) return redirect("/");

        $cNombre = $request->cNombre ;
        $cUrl = $request->cUrl;

        $cNombre = $request->cNombre == '' ? "":$cNombre;
        $cUrl = $request->cUrl == ''  ? "":$cUrl;

        $Datos =  DB::select("call sp_Rol_setRegistrarRol(?,?)",[$cNombre,$cUrl]);
        $IdRol = $Datos[0]->IdRol;

        try {
            DB::beginTransaction();
            $listPermisos= $request->listPermisosFilter;
            if(sizeof($listPermisos)>0){
                foreach($listPermisos as $key=>$value){
                    if($value['checked']){
                        $Datos =  DB::select("call sp_Rol_setRegistrarRolByPermiso(?,?)",[$IdRol,$value['id']]);
                    }   
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return['status'=>500,'msg'=>$e];
        }
    } 

    public function setEditarRolPermisos(Request $request){
        if(!$request->ajax()) return redirect("/");

        $IdRol = $request->IdRol;
        $cNombre = $request->cNombre ;
        $cUrl = $request->cUrl;

        $IdRol = $request->IdRol == '' ? "0":$IdRol;
        $cNombre = $request->cNombre == '' ? "":$cNombre;
        $cUrl = $request->cUrl == ''  ? "":$cUrl;

        $Datos =  DB::select("call sp_Rol_setActualizarRol(?,?,?)",[$cNombre,$cUrl,$IdRol]);

        try {
            DB::beginTransaction();
            $listPermisos= $request->listPermisosFilter;
            if(sizeof($listPermisos)>0){
                foreach($listPermisos as $key=>$value){
                    if($value['checked']){
                        $Datos =  DB::select("call sp_Rol_setRegistrarRolByPermiso(?,?)",[$IdRol,$value['id']]);
                    }   
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return['status'=>500,'msg'=>$e];
        }
    }

    public function ObtenerRolByUsuario(Request $request){
        if(!$request->ajax()) return redirect("/");

        $nIdUser = $request->id;
        $nIdUser = $request->id == '' ? "0":$request->id;
        $Datos =  DB::select("call sp_Rol_getRolByUser(?)",[$nIdUser]);
        
        return ['rol'=>$Datos];
    }
}
