<?php

namespace App\Http\Controllers\Administracion;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;
class UserController extends Controller
{
    public function ObtenerListaUsuarios(Request $request){
        if(!$request->ajax()) return redirect("/");
        $cNombre = $request->cNombre;
        $cUsuario = $request->cUsuario;
        $cCorreo = $request->cCorreo;
        $cEstado = $request->cEstado;

        $cNombre = $request->cNombre == null ? '':$cNombre;
        $cUsuario = $request->cUsuario == null  ? '':$cUsuario;
        $cCorreo = $request->cCorreo == null  ? '':$cCorreo ;
        $cEstado = $request->cEstado == null ? '': $cEstado;

        $Datos =  DB::select("call sp_Usuario_getListaUsuarios(?,?,?,?)",[$cNombre,$cUsuario,$cCorreo,$cEstado]);
        
        return [
            'usuarios'=>$Datos
        ];
    }

    public function RegistrarUsuario(Request $request)
    {
        if(!$request->ajax()) return redirect("/");

        $cNombre= $request->cNombre == null ? '':$request->cNombre;
        $cSegundoNombre= $request->cSegundoNombre == null  ? '': $request->cSegundoNombre;
        $cApellidos= $request->cApellidos == null  ? '': $request->cApellidos;
        $cUsuario= $request->cUsuario == null  ? '': $request->cUsuario;
        $cCorreo= $request->cCorreo == null  ? '':$request->cCorreo;
        $cContrasena= Hash::make($request->cContrasena);
        $cImagen = $request->cImagen == null  ? '':$request->cImagen;
        $rta =  DB::select("call sp_registrarUsuario(?,?,?,?,?,?,?)",[$cNombre,$cSegundoNombre,$cApellidos,$cUsuario,$cCorreo,$cContrasena,$cImagen]);
        return ['nIdUsuario'=>$rta[0]->nIdUsuario];
    }

    public function ObtenerUsuario(Request $request){
        if(!$request->ajax()) return redirect("/");

        $Id = $request->id; 
        $rta =  DB::select("call sp_ObtenerUsuario(?)",[$Id]);
        return ['usuario'=>$rta];
    }

    public function ActualizarUsuario(Request $request){
        if(!$request->ajax()) return redirect("/");

        $id = $request->nId;
        $cNombre= $request->cNombre == null ? '':$request->cNombre;
        $cSegundoNombre= $request->cSegundoNombre == null  ? '': $request->cSegundoNombre;
        $cApellidos= $request->cApellidos == null  ? '': $request->cApellidos;
        $cUsuario= $request->cUsuario == null  ? '': $request->cUsuario;
        $cCorreo= $request->cCorreo == null  ? '':$request->cCorreo;
        $cContrasena= Hash::make($request->cContrasena);
        $cImagen = $request->cImagen == null  ? '':$request->cImagen;
        $IdUser =1;
        $rta =  DB::select("call sp_actualizarUsuario(?,?,?,?,?,?,?,?,?)",[$id,$cNombre,$cSegundoNombre,$cApellidos,$cUsuario,$cCorreo,$cContrasena,$cImagen,$IdUser]);
        return [$rta];
    }

    public function Activar(Request $request){
        if(!$request->ajax()) return redirect("/");

        $Id = $request->id; 
        try{
            $rta =  DB::select("call sp_ActivarUsuario(?,?)",[1,$Id]);
            return ['status'=>200];
        }
        catch(Exception $e){
            return ['error'=>$e];
        }
        
    }

    public function InActivar(Request $request){
        if(!$request->ajax()) return redirect("/");

        $Id = $request->id; 
        try{
            $rta =  DB::select("call sp_ActivarUsuario(?,?)",[0,$Id]);
            return ['status'=>200];
        }
        catch(Exception $e){
            return ['error'=>$e];
        }
    }

    public function registrarRolByUsuario(Request $request){
        if(!$request->ajax()) return redirect("/");
        try{
            $nIdUsuario= $request->nIdUsuario == null ? '0':$request->nIdUsuario;
            $nIdRol= $request->nIdRol == null  ? '0': $request->nIdRol;
            $rta =  DB::select("call sp_usuario_registrarRolByUsuario(?,?)",[$nIdUsuario,$nIdRol]);
            return [$rta];
        }
        catch(Exception $e){
            return ['error'=>$e];
        }
    }
}
