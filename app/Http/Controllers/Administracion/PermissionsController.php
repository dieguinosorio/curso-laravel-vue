<?php

namespace App\Http\Controllers\Administracion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PermissionsController extends Controller
{
    public function getListarPermisos(Request $request){
        if(!$request->ajax()) return redirect("/");

        $nIdPermiso = $request->nIdPermiso;
        $cNombre = $request->cNombre ;
        $cUrl = $request->cUrl;

        $nIdPermiso = $request->nIdPermiso == '' ? "0":$request->nIdPermiso;
        $cNombre = $request->cNombre == '' ? "":$cNombre;
        $cUrl = $request->cUrl == ''  ? "":$cUrl;

        $Datos =  DB::select("call sp_Rol_getListaPermisos(?,?,?)",[$cNombre,$cUrl,$nIdPermiso]);
        
        return ['permisos'=>$Datos];
    }

    public function setRegistrarPermiso(Request $request){
        if(!$request->ajax()) return redirect("/");
        try{
            DB::beginTransaction();
            $cNombre = $request->cNombre ;
            $cUrl = $request->cUrl;

            $cNombre = $request->cNombre == '' ? "":$cNombre;
            $cUrl = $request->cUrl == ''  ? "":$cUrl;

            $Datos =  DB::select("call sp_permiso_setRegistrarPermiso(?,?)",[$cNombre,$cUrl]);
            DB::commit();
            return ['status'=>'200','msg'=>"Permiso Registrado con Exito"];
        }
        catch(Exception $e){
            DB::rollBack();
            return['status'=>500,'msg'=>$e];
        }
    }

    public function getPermiso(Request $request){
        if(!$request->ajax()) return redirect("/");
        try{
            $nIdPermiso = $request->id ;
            $nIdPermiso = $request->id == '' ? "0":$nIdPermiso;

            $Datos =  DB::select("call sp_permiso_getPermiso(?)",[$nIdPermiso]);
            return ['permiso'=>$Datos];
        }
        catch(Exception $e){
            return['status'=>500,'msg'=>$e];
        }
    }

    public function ObtenerPermisosByUsuario(Request $request){
        if(!$request->ajax()) return redirect("/");
        try{
            
            $nIdUser = $request->id ;
            $nIdUser = $request->id == '' ? "0":$nIdUser;
            if(!$request->id ){
                $nIdUser = \Auth::user()->id;
            }
            $Datos =  DB::select("call sp_permiso_getPermisosByUser(?)",[$nIdUser]);
            return ['permisos'=>$Datos];
        }
        catch(Exception $e){
            return['status'=>500,'msg'=>$e];
        }
    }

    public function setEditarPermiso(Request $request){
        if(!$request->ajax()) return redirect("/");
        try{
            DB::beginTransaction();
            $nIdPermiso = $request->nIdPermiso ;
            $cNombre = $request->cNombre ;
            $cUrl = $request->cUrl;

            $nIdPermiso = $request->nIdPermiso == '' ? "":$nIdPermiso;
            $cNombre = $request->cNombre == '' ? "":$cNombre;
            $cUrl = $request->cUrl == ''  ? "":$cUrl;

            $Datos =  DB::select("call sp_permiso_setEditarPermiso(?,?,?)",[$cNombre,$cUrl,$nIdPermiso]);
            DB::commit();
            return ['status'=>'200','msg'=>"Permiso Editado con Exito"];
        }
        catch(Exception $e){
            DB::rollBack();
            return['status'=>500,'msg'=>$e];
        }
    }

    public function setActualizarPermisosByUsuario(Request $request){
        try {
            DB::beginTransaction();
            $nIdUser = $request->cnIdUser;
            $nIdUser = $nIdUser >0 ? $nIdUser : 0;
            $listPermisos= $request->listPermisosFilter;
            $Entro = 0;
            if(sizeof($listPermisos)>0){
                $Datos = DB::select("call sp_Permiso_setRegistrarPermisosByUsuario(?,?,?)",[$nIdUser,0,1]);
                foreach($listPermisos as $key=>$value){
                    if($value['checked']){
                        $Id =$value['id'];
                        $Datos = DB::select("call sp_Permiso_setRegistrarPermisosByUsuario(?,?,?)",[$nIdUser,$Id,0]);
                    }   
                }
            }
            DB::commit();
            return['status'=>201,'msg'=>$Datos];
        } catch (Exception $e) {
            DB::rollBack();
            return['status'=>500,'msg'=>$e];
        }
    }

    public function LisRolObtenerPermisosByUsuario(Request $request)
    {
        if(!$request->ajax()) return redirect("/");
        try{
            $nIdUser = $request->id ;
            $nIdUser = $request->id == '' ? "0":$nIdUser;

            if(!$request->id){
                $nIdUser = \Auth::user()->id;
            }
            $Datos =  DB::select("call sp_permiso_getRolPermisosByUser(?)",[$nIdUser]);
            return ['permisos'=>$Datos];
        }
        catch(Exception $e){
            return['status'=>500,'msg'=>$e];
        }
    }
}
