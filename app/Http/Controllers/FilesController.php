<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class FilesController extends Controller
{
    public function Registrar(Request $request)
    {
        $file       =   $request->file;
        $bandera    =   str_random(10);
        $filename   =   $file->getClientOriginalName();
        $fileserver =   $bandera .'_'. $filename;
        Storage::putFileAs('public/users', $file, $fileserver);

        $rpta       =   DB::select('call sp_RegistrarArchivo (?, ?)',[asset('storage/users/'. $fileserver),$filename]);
        return $rpta;
    }

    public function EliminarImagenAnterior(Request $request)
    {
        $Delete='';
        
        if(isset($request->cImagen) && $request->cImagen !=''){
            $ImageUrl = '/users/'.$request->cImagen;
            $Delete = Storage::disk('public')->delete($ImageUrl);
        }
        return ['deleted'=>$request->cImagen];
    }
}
