<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Rutas login

Route::post('/authenticated/login','Auth\LoginController@login');
Route::post('/authenticated/logout','Auth\LoginController@logout');
Route::get('/authenticated/getRefrescarUsaurioAutentificado',function(){
    return \Auth::user()->load('file');
});
//Rutas Usuarios
Route::get('/administracion/usuario/getListUsuario','Administracion\UserController@ObtenerListaUsuarios');
Route::post('/archivo/registrarArchivo','FilesController@Registrar');
Route::post('/administracion/usuario/registrar','Administracion\UserController@RegistrarUsuario');
Route::put('/administracion/usuario/actualizar','Administracion\UserController@ActualizarUsuario');
Route::get('/administracion/usuario/getUsuario/{id}','Administracion\UserController@ObtenerUsuario');
Route::put('/administracion/usuario/inactivar','Administracion\UserController@InActivar');
Route::put('/administracion/usuario/activar','Administracion\UserController@Activar');
Route::post('/administracion/usuario/registrarRolByUsuario','Administracion\UserController@registrarRolByUsuario');
Route::get('/archivo/obtenerArchivo/{id}','FilesController@ObtenerImagen');
Route::post('/archivo/eliminarImagenPerfil','FilesController@EliminarImagenAnterior');
Route::get('/administracion/permiso/LisRolObtenerPermisosByUsuario/{id?}','Administracion\PermissionsController@LisRolObtenerPermisosByUsuario');

//Lista roles
Route::get('/administracion/roles/getListRoles','Administracion\RolesController@ObtenerListaRoles');
Route::get('/administracion/roles/getListarPermisosByRol','Administracion\RolesController@getListarPermisosByRol');
Route::get('/administracion/roles/getListarPermisosByIdRol','Administracion\RolesController@getListarPermisosByIdRol');
Route::post('/administracion/roles/setRegistrarRolPermiso','Administracion\RolesController@setRegistrarRolPermiso');
Route::post('/administracion/roles/setEditarRolPermisos','Administracion\RolesController@setEditarRolPermisos');
Route::get('/administracion/roles/ObtenerRolByUsuario/{id}','Administracion\RolesController@ObtenerRolByUsuario');

//Permisos
Route::get('/administracion/permiso/getListarPermisos','Administracion\PermissionsController@getListarPermisos');
Route::post('/administracion/permiso/setRegistrarPermiso','Administracion\PermissionsController@setRegistrarPermiso');
Route::get('/administracion/permiso/getPermiso/{id}','Administracion\PermissionsController@getPermiso');
Route::post('/administracion/permiso/setEditarPermiso','Administracion\PermissionsController@setEditarPermiso');
Route::get('/administracion/permiso/ObtenerPermisosByUsuario/{id?}','Administracion\PermissionsController@ObtenerPermisosByUsuario');//
Route::post('/administracion/permiso/setActualizarPermisosByUsuario','Administracion\PermissionsController@setActualizarPermisosByUsuario');
Route::get('/{optional?}', function () {
    return view('app');
})->name('basepath')->where('optional','.*');
