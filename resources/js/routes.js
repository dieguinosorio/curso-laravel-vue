import Vue from 'vue';
import Router from 'vue-router';
Vue.use(Router)
import Dashboard from './components/modulos/dashboard/index.vue';
import Cliente from './components/modulos/clientes/index.vue';
import Categoria from './components/modulos/categorias/index.vue';
import Pedido from './components/modulos/pedidos/index.vue';
import Permisos from './components/modulos/permisos/index.vue';
import CrearPermisos from './components/modulos/permisos/create.vue';
import EditarPermiso from './components/modulos/permisos/edit.vue';

import Productos from './components/modulos/productos/index.vue';
import Reportes from './components/modulos/reportes/index.vue';
import Usuario from './components/modulos/usuarios/index.vue';
import UsuarioPermiso from './components/modulos/usuarios/permission.vue';


import Perfil from './components/modulos/usuarios/profile.vue';
import Roles from './components/modulos/roles/index.vue';
import CrearRoles from './components/modulos/roles/create.vue';
import EditarRol from './components/modulos/roles/edit.vue';
import Login from './components/modulos/authenticated/login.vue';
import Pagina404 from './components/plantilla/404.vue';

function ValidarAcceso(to, from, next) {
    let authUser = JSON.parse(sessionStorage.getItem('authUser'));
    console.log(JSON.parse(sessionStorage.getItem('authUser')))
    if (authUser) {
        let listaPermisosByUser = JSON.parse(sessionStorage.getItem('listPermisosFilterByRolUser'));
        if (listaPermisosByUser.includes(to.name)) {
            next();
        } else {
            let listRolPermisosByUsuarioFilter = [];
            listaPermisosByUser.map(function(x) {
                if (x.includes('index')) {
                    listRolPermisosByUsuarioFilter.push(x);
                }
            })
            if (to.name == 'dashboard.index') {
                next({ name: listRolPermisosByUsuarioFilter[0] })
            } else {
                next(from.path);
            }
        }
    }
}

export default new Router({
    routes: [{
            path: '/login',
            component: Login,
            name: 'login',
        },
        {
            path: '/',
            component: Dashboard,
            name: 'dashboard.index',
            beforeEnter: (to, from, next) => {
                ValidarAcceso(to, from, next);
            }
        },
        {
            path: '/cliente',
            component: Cliente,
            name: 'cliente.index',
            beforeEnter: (to, from, next) => {
                ValidarAcceso(to, from, next);
            }
        },

        {
            path: '/categoria',
            component: Categoria,
            name: 'categoria.index',
            beforeEnter: (to, from, next) => {
                ValidarAcceso(to, from, next);
            }
        },

        {
            path: '/pedido',
            component: Pedido,
            name: 'pedido.index',
            beforeEnter: (to, from, next) => {
                ValidarAcceso(to, from, next);
            }
        },

        {
            path: '/permiso',
            component: Permisos,
            name: 'permiso.index',
            beforeEnter: (to, from, next) => {
                ValidarAcceso(to, from, next);
            }
        },

        {
            path: '/permiso/crear',
            component: CrearPermisos,
            name: 'permiso.crear',
            beforeEnter: (to, from, next) => {
                ValidarAcceso(to, from, next);
            }
        },

        {
            path: '/permiso/editar/:id',
            component: EditarPermiso,
            name: 'permiso.editar',
            props: true,
            beforeEnter: (to, from, next) => {
                ValidarAcceso(to, from, next);
            }
        },

        {
            path: '/producto',
            component: Productos,
            name: 'producto.editar',
            beforeEnter: (to, from, next) => {
                ValidarAcceso(to, from, next);
            }
        },
        {
            path: '/reporte',
            component: Reportes,
            name: 'reporte.index',
            beforeEnter: (to, from, next) => {
                ValidarAcceso(to, from, next);
            }
        },
        {
            path: '/usuario',
            component: Usuario,
            name: 'usuario.index',
            beforeEnter: (to, from, next) => {
                ValidarAcceso(to, from, next);
            }
        },
        {
            path: '/usuario/permiso/:id',
            component: UsuarioPermiso,
            name: 'usuario.permiso',
            props: true,
            beforeEnter: (to, from, next) => {
                ValidarAcceso(to, from, next);
            }
        },

        {
            path: '/perfil/:id',
            component: Perfil,
            name: 'perfil',
            props: true,
            beforeEnter: (to, from, next) => {
                ValidarAcceso(to, from, next);
            }
        },
        {
            path: '/roles',
            component: Roles,
            name: 'roles.index',
            beforeEnter: (to, from, next) => {
                ValidarAcceso(to, from, next);
            }
        },
        {
            path: '/roles/crear',
            component: CrearRoles,
            name: 'roles.crear',
            beforeEnter: (to, from, next) => {
                ValidarAcceso(to, from, next);
            }
        },
        {
            path: '/roles/editar/:id',
            component: EditarRol,
            name: 'rol.editar',
            props: true,
            beforeEnter: (to, from, next) => {
                ValidarAcceso(to, from, next);
            }
        },

        {
            path: '*',
            component: Pagina404,
            name: '404.index'
        },
    ],
    mode: 'history',
    linkActiveClass: 'active'
});